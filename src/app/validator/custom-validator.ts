import {FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators, AbstractControl} from '@angular/forms';


// export const matchPassword = (control: AbstractControl): { [key: string]: any } => {
//     const password = control.get('password');
//     const confirmPassword = control.get('confirmPassword');
//     console.log(password, confirmPassword)
//     if (!password || !confirmPassword) return null;
//     if (password.value === confirmPassword.value) {
//         return null;
//     } else {
//         return 'password and confirm password must match';
//     }
// };
export class CustomValidators {

  static birthYear(c: FormControl): ValidationErrors {
    const numValue = Number(c.value);
    const currentYear = new Date().getFullYear();
    const minYear = currentYear - 85;
    const maxYear = currentYear - 18;
    const isValid = !isNaN(numValue) && numValue >= minYear && numValue <= maxYear;
    const message = {
      'years': {
        'message': 'The year must be a valid number between ' + minYear + ' and ' + maxYear
      }
    };
    return isValid ? null : message;
  }

  static matchPassword(form: FormGroup): ValidationErrors {
    const password = form.get('password');
    const confirmPassword = form.get('newPassword');

    let error;
    if (password && confirmPassword) {
      // console.log(password.value, confirmPassword.value)
      if (password.value !== confirmPassword.value) {
        error = 'Password and confirm password must match.'
      }
      const message = {
        'matchPassword': {
          'message': error
        }
      };
      // console.log(error)

      return error ? message : null;
    }
  }

  static countryCity(form: FormGroup): ValidationErrors {
    const countryControl = form.get('country');
    const cityControl = form.get('city');

    if (countryControl != null && cityControl != null) {
      const country = countryControl.value;
      const city = cityControl.value;
      // console.log(country)
      // console.log(city)
      let error = null;

      if (country === 'France' && city !== 'Paris') {
        console.log(country, city)
        error = 'If the country is France, the city must be Paris';
      }

      const message = {
        'countryCity': {
          'message': error
        }
      };

      return error ? message : null;
    }
  }

  static uniqueName(c: FormControl): Promise<ValidationErrors> {
    const message = {
      'uniqueName': {
        'message': 'The name is not unique'
      }
    };

    return new Promise(resolve => {
      setTimeout(() => {
        resolve(c.value === 'Existing' ? message : null);
      }, 1000);
    });
  }

  static strongPassword(c: FormControl): ValidationErrors {
    const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/.test(c.value);
    const isSpace = /^\S(.*\S)?$/.test(c.value);
    const isValidLength = /^[^]{6,}$/.test(c.value);
    const message = {
      'strongPassword': {
        'message': 'Please complete strong password requirment'
      }
    };
    return isValid && isValidLength && isSpace ? null : message;
  }

  static checkCC(c: FormControl): ValidationErrors {
    let isValid
    let regexVisa = /^4[0-9]{12}(?:[0-9]{3})?$/;
    let regexMaster = /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/;
    let regexAmericanExp = /^3[47][0-9]{13}$/;
    let regexDiscover = /^6(?:011|5[0-9]{2})[0-9]{12}$/;

    if (regexVisa.test(c.value)) {
      isValid = true;
    } else {
      if (regexMaster.test(c.value)) {
        isValid = true;
      } else {
        if (regexAmericanExp.test(c.value)) {
          isValid = true;
        } else {
          if (regexDiscover.test(c.value)) {
            isValid = true;
          } else {
            isValid = false;
          }
        }
      }
    }
    const message = {
      'validCC': {
        'message': 'Please enter valid card number'
      }
    };
    return isValid ? null : message;
  }

  static telephoneNumber(c: FormControl): ValidationErrors {
    const isValidPhoneNumber = /^\d{3,3}-\d{3,3}-\d{3,3}$/.test(c.value);
    const message = {
      'telephoneNumber': {
        'message': 'The phone number must be valid (XXX-XXX-XXX, where X is a digit)'
      }
    };
    return isValidPhoneNumber ? null : message;
  }

  static telephoneNumbers(form: FormGroup): ValidationErrors {

    const message = {
      'telephoneNumbers': {
        'message': 'At least one telephone number must be entered'
      }
    };

    const phoneNumbers = <FormArray>form.get('phoneNumbers');
    const hasPhoneNumbers = phoneNumbers && Object.keys(phoneNumbers.controls).length > 0;

    return hasPhoneNumbers ? null : message;
  }

  static validateAllFormFields(formGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      // console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        // console.log(control)
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        // console.log(control)
        this.validateAllFormFields(control);
      } else if (control instanceof FormArray) {
        // console.log(control)
        this.validateAllFormFields(control);
      }
    });
  }
}
