import { Injectable } from '@angular/core';
import { ConstantsHelper } from '../constants/constant';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class QuizHttpService {

  constructor(private http:HttpClient , private router:Router) { }

  public makeRequestApi(type,urlName,payload?,params?){
    const apiUrl = ConstantsHelper.getAPIUrl[urlName](params);
    switch (type){
      case 'get':
        const headerOpt = payload? {} : {
          headers:{
            apiKey : 'OnlineQuiz'
          }
        };
        return this.http.get(apiUrl, headerOpt).pipe(
          tap((result: any) => {

          }),
          catchError(this.handleError())
        );
        case 'post':
          return this.http.post(apiUrl, JSON.stringify(payload))
          // .retry(0)
          // .timeout(8000)
            .pipe(
              tap((result: any) => {
                // this.extractData(result)
              }),
              catchError(this.handleError())
            );
    }
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // this.router.navigate(['/login']);
      let errorMessage = null;
      if (error.error instanceof ErrorEvent) {
        errorMessage = 'An error occurred:' + ',' + error.error.message;
      } else {
        // console.log(error.status);

        if (error.status === 400) {
          // this.alertService.swalError('Your session has been expired. Please login again.');
          this.router.navigate(['/homeAdmin']);
        }
        if (error.status === 401) {
          // this.alertService.swalError('Unauthorised user!');
          this.router.navigate(['/homeAdmin']);
        }
        if (error.status === 500) {
          // this.alertService.swalError('Got some trouble, please try again!');
          // this.router.navigate(['/landing']);
        }
        errorMessage = `${error.error.message}`;
      }
      // this.log(`${operation} failed: ${error.message}`);
      // this.alertService.swalError();
      return throwError(errorMessage = errorMessage ? errorMessage : 'Something bad happened; please try again later.');
    };
  }
}
