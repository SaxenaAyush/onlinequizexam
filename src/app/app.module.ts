import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';
import { ClientDashboardComponent } from './components/Client/client-dashboard/client-dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { ChartsModule } from 'ng2-charts';
import { ManageQuestionComponent } from './components/Admin/manage-question/manage-question.component';
import { AdminHomeComponent } from './components/Admin/admin-home/admin-home.component';
import { ManageUserComponent } from './components/Admin/manage-user/manage-user.component';
// import { ModalModule } from 'ngx-modal';
import { ProgressBarModule } from "angular-progress-bar";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgxSelectModule } from 'ngx-select-ex';
import { HighchartsChartModule } from 'highcharts-angular';
import { SharedModule } from './common/sharaed/sharaed.module';
import { NgxSpinnerModule } from "ngx-spinner";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ManageAccountComponent } from './components/manage-account/manage-account.component';
import { ViewReportComponent } from './components/Client/view-report/view-report.component';
import { InstructionComponent } from './components/Client/instruction/instruction.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddQuizComponent } from './components/Admin/add-quiz/add-quiz.component';
import { ViewClientsComponent } from './components/Admin/view-clients/view-clients.component';
import { AddQuestionComponent } from './components/Admin/add-question/add-question.component';
import { ViewEngagementComponent } from './components/Admin/view-engagement/view-engagement.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { SubmitComponent } from './components/Client/submit/submit/submit.component';
import { ClientPanelComponent } from './components/Client/client-panel/client-panel.component';
import { AddEngagementComponent } from './components/Admin/add-engagement/add-engagement.component';
import { AdminDashboardComponent } from './components/Admin/admin-dashboard/admin-dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    AdminDashboardComponent,
    ClientDashboardComponent,
    HomeComponent,
    FooterComponent,
    SideBarComponent,
    ManageQuestionComponent,
    AdminHomeComponent,
    ManageUserComponent,
    ManageAccountComponent,
    ViewReportComponent,
    InstructionComponent,
    AddQuizComponent,
    ViewClientsComponent,
    AddQuestionComponent,
    ViewEngagementComponent,
    HomeAdminComponent,
    SubmitComponent,
    ClientPanelComponent,
    AddEngagementComponent
  ],
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    NgxSelectModule,
    SharedModule.forRoot(),
    NgxSpinnerModule,
    HighchartsChartModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
