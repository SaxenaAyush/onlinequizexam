import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';
import { AdminDashboardComponent } from './components/Admin/admin-dashboard/admin-dashboard.component';
import { ClientDashboardComponent } from './components/Client/client-dashboard/client-dashboard.component';
import { HomeComponent } from './components/home/home.component';

import { AdminHomeComponent } from './components/Admin/admin-home/admin-home.component';
import { ManageUserComponent } from './components/Admin/manage-user/manage-user.component';
import { ManageAccountComponent } from './components/manage-account/manage-account.component';
import { ViewReportComponent } from './components/Client/view-report/view-report.component';
import { InstructionComponent } from './components/Client/instruction/instruction.component';
import { AddQuizComponent } from './components/Admin/add-quiz/add-quiz.component';
import { ViewClientsComponent } from './components/Admin/view-clients/view-clients.component';
import { AddQuestionComponent } from './components/Admin/add-question/add-question.component';
import { ViewEngagementComponent } from './components/Admin/view-engagement/view-engagement.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { SubmitComponent } from './components/Client/submit/submit/submit.component';
import { ClientPanelComponent } from './components/Client/client-panel/client-panel.component';
import { ManageQuestionComponent } from './components/Admin/manage-question/manage-question.component';
import { AuthGuard } from './guard/auth.guard';
import { AddEngagementComponent } from './components/Admin/add-engagement/add-engagement.component';
// import { AuthGuard } from './guard/auth.guard';



const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
    
     
    ]
  },
 
  {
    path: 'admin',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: AdminDashboardComponent
      },
      {
        path: 'manageQuestion',
        component: ManageQuestionComponent
      },
      {
        path: 'addQuiz',
        component: AddQuizComponent
      },
      {
        path: 'home',
        component: AdminHomeComponent
      },
      {
        path: 'manage-user',
        component: ManageUserComponent
      },
      {
        path: 'viewUser',
        component: ViewClientsComponent
      },
      {
        path: 'addQuestion',
        component: AddQuestionComponent
      },
      {
        path: 'viewEng',
        component: ViewEngagementComponent
      },
      // {
      //   path: 'addEng',
      //   component: AddEngagementComponent
      // },
    ]
  },
  
  {
    path: 'client',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        component: ClientDashboardComponent
      },
      {
        path: 'instruction',
        component: InstructionComponent
      },
      {
        path: 'viewReport',
        component:ViewReportComponent
      },
      {
        path: 'confirm',
        component: SubmitComponent
      },
      {
        path: 'panel',
        component: ClientPanelComponent
      },
     
    ]
  },
  {
    path : 'manageAccount',
    component: ManageAccountComponent
  },
  {
    path: 'homeAdmin',
    component : HomeAdminComponent,
    

  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
