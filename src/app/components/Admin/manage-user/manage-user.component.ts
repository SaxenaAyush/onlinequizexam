import { Component, OnInit, ViewChild } from '@angular/core';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import axios from 'axios';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.css']
})
export class ManageUserComponent implements OnInit {
  public userForm: FormGroup;
  tokenValue = this.storageService.get('token');
  page = 1;
  pageSize = 7;
  @ViewChild('closebutton', { static: false }) closebutton;
  constructor(private storageService: QuizStorageService, private sweetAlert: SweetAlertService, private fb: FormBuilder,
    private quizHttpService: QuizHttpService, private router: Router, private SpinnerService: NgxSpinnerService) { }
  loggedInUserDetails: any;
  dataResponse: any;
  userList: any[] = [];
  userDetails: any;
  ngOnInit() {
    this.initForm();
    this.viewUser();
    this.loggedInUserDetails = this.storageService.get('currentUser');
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });
  }

  initForm() {
    this.userForm = this.fb.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      phone_no: ['', [Validators.required]],
      user_type: ['', [Validators.required]],
      is_active: true

    });
  }

  save() {
    const reqMap = {
      ...this.userForm.value,

    };
    // console.log(reqMap)
    axios.post('http://env-9498608.cloudjiffy.net/api/register', reqMap)
      .then((response) => {
        if (response.data.code == 200) {
          this.dataResponse = response;

          console.log(this.dataResponse.success, 'check');

          this.closebutton.nativeElement.click();
          this.sweetAlert.swalSuccess('User Added Successfully')
        } else if (response.data.code != 200) {
          this.sweetAlert.swalError('error occurred')

        }
      })
      .catch(function (error) {
        console.log(error);
      });
    this.viewUser();

  }

  viewUser() {
    this.SpinnerService.show();

    axios.get('http://env-9498608.cloudjiffy.net/api/list_users', { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.userList = response.data.list_users;

        console.log(this.userList, 'UserList');


        this.SpinnerService.hide();

      })
  }
  deleteUser(emp) {
    console.log(emp, "id")
    const reqMap = {
      "id": emp

    };
    axios.post('http://env-9498608.cloudjiffy.net/api/delete_users', reqMap, { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.userList = response.data.list_users;

        console.log(this.userList, 'UserList');



      })
    this.viewUser();
  }
  viewDetails(emp) {
    const reqMap = {
      "id": emp

    };
    axios.post('http://env-9498608.cloudjiffy.net/api/view_users', reqMap, { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.userDetails = response.data.list_users;

        console.log(this.userDetails, 'userDetails');

        this.storageService.set('userdetails', this.userDetails);
        this.router.navigate(['/admin/viewUser']);

      })
    this.viewUser();
  }
}
