import { Component, OnInit } from '@angular/core';
import { ExcelServicesService } from 'src/app/service/excel-service.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';

@Component({
  selector: 'app-view-engagement',
  templateUrl: './view-engagement.component.html',
  styleUrls: ['./view-engagement.component.css']
})
export class ViewEngagementComponent implements OnInit {
  excel = [
    { SNO: 1, Name: "Mark", Email: "Mark@email.com", Phone: "9067896745", Score: 90, Percentage: "90%" },
    { SNO: 2, Name: "Jacob", Email: "Jacob@email.com", Phone: "9067896745", Score: 90, Percentage: "90%" },
    { SNO: 3, Name: "Larry", Email: "Larry@email.com", Phone: "9067896745", Score: 90, Percentage: "90%" },
    { SNO: 3, Name: "Larry", Email: "Larry@email.com", Phone: "9067896745", Score: 90, Percentage: "90%" },
    { SNO: 3, Name: "Larry", Email: "Larry@email.com", Phone: "9067896745", Score: 90, Percentage: "90%" },
  ];
  constructor(private storageService: QuizStorageService, private excelService: ExcelServicesService, private http: HttpClient, private alertService: SweetAlertService) {
    this.getJSON().subscribe(data => {
      data.forEach(row => {
        this.excel.push(row);
      });
    });
  }
  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.excel, 'sample');
    this.alertService.swalSuccess('Excel Succesfully Downloaded');

  }
  public getJSON(): Observable<any> {
    return this.http.get('https://api.myjson.com/bins/zg8of');
  }
  ngOnInit() {
    this.view_engagment();
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });
  }

  viewEngagment: any;
  Quiz_Info: any;
  listofusers: any = [];
  TopFive: any = [];
  Greater_than_70: any = [];
  Less_than_30: any = [];
  view_engagment() {
    this.viewEngagment = this.storageService.get('anagment');
    this.Quiz_Info = this.viewEngagment.Quiz_Info;
    this.listofusers = this.viewEngagment.listofusers;
    this.TopFive = this.viewEngagment.TopFive;
    this.Greater_than_70 = this.viewEngagment.Greater_than_70;
    this.Less_than_30 = this.viewEngagment.Less_than_30;
    console.log(this.viewEngagment, 'check view');
    console.log(this.Quiz_Info, 'check Quiz_Info');
    console.log(this.listofusers, 'check listofusers');
    console.log(this.TopFive, 'check TopFive');
    console.log(this.Greater_than_70, 'check Greater_than_70');
    console.log(this.Less_than_30, 'check Less_than_30');
  }

}
