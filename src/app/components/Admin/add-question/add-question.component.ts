import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { home, option } from '../home/home.model';
import { Router } from '@angular/router';
import axios from 'axios';
@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {
  myvalue: number = 0;
  home = new home()
  option = new option()
  categoryList: any[] = [];
  public singleoptions: any[] = [{
    option: '',

  }];
  public multipleoptions: any[] = [{
    option: '',

  }];
  dataArray = [];
  optionArray = [];
  questionModel: any = [{
    "question": "what is age",
    "question_type": "Question Type",
    "option_list": [""],
    "required": true
  }];

  optionList: any = [
    // {
    //   "name":"----select Type----",
    //   "icon":"option_1.svg"
    // },

    // {
    //   "name":"Open Ended",
    //   "icon":"option_2.svg"
    // },
    // {
    //   "name":"Multiple Choice",
    //   "icon":"option_3.svg"
    // },


  ]
  answerList: any = [
    {
      "name": "----select Answer----",
      "icon": "option_1.svg"
    },
    {
      "name": "Option 1",
      "icon": "option_1.svg"
    },
    {
      "name": "Option 2",
      "icon": "option_1.svg"
    },
  ]
  constructor(private router: Router) { }

  ngOnInit() {
    this.question_type();
    this.dataArray.push(this.home)
    this.optionArray.push(this.option)
    this.categoryList = this.optionList;
    console.log(this.categoryList);
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');

    });

  }



  question_type() {
    axios.get('http://env-9498608.cloudjiffy.net/api/question_type')
      .then((response) => {

        console.log(response, 'response');
        this.optionList = response.data.list_question_type;

        console.log(this.optionList, 'optionList');
      })
  }

  addQuestion() {
    this.questionModel.push({
      "question": "",
      "question_type": "",
      "option_list": [],
      "required": false
    });

  }

  addForm() {
    this.home = new home();
    this.dataArray.push(this.home);

  }
  removeform(index) {
    this.dataArray.splice(index);
  }


  // addOption(index){
  //   this.myvalue = this.myvalue+1;
  //   this.questionModel[0].option_list.push(''+this.myvalue);
  //   }

  removeOption(index) {
    // debugger;

    this.questionModel[0].option_list.splice(index, 1);
  }

  clickOption(index, value) {
    this.questionModel[index].question_type = value
  }


  onChange(deviceValue) {
    console.log(deviceValue, 'check value ');
    if (deviceValue == 2) {
      $('.multiple').show();
      $('.singleChoice').hide();
      $('.fillups').hide();
      $('.addButton').show();
      $('.addButton2').hide();

      console.log('sahi hai')
    }
    else if (deviceValue == 1) {
      $('.multiple').hide();
      $('.singleChoice').show();
      $('.fillups').hide();
      $('.addButton').hide();
      $('.addButton2').show();
      console.log('sahi hai')
    }
    else if (deviceValue == 'Short') {
      $('.multiple').hide();
      $('.singleChoice').hide();
      $('.fillups').show();
      $('.addButton').hide();
      $('.addButton2').hide();
      console.log('sahi hai')
    }
  }
  OptionAdd() {
    this.option = new option();

    this.optionArray.push(this.option);
  }
  OptionRemove(index) {
    this.optionArray.splice(index);
  }
  save() {
    alert('Quiz has been saved');
    this.router.navigate(['admin/addQuiz']);
  }
  addmultiple() {
    this.multipleoptions.push({
      option: '',

    });
  }
  addsingle() {
    this.singleoptions.push({
      option: '',

    });
  }
  removemultiple(i: number) {
    this.multipleoptions.splice(i, 1);
  }
  removesingle(i: number) {
    this.singleoptions.splice(i, 1);
  }
}
