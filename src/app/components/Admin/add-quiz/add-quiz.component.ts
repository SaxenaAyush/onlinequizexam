import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as $ from '../../../../../node_modules/jquery';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import axios from 'axios';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent implements OnInit {
  public addQuizForm: FormGroup;
  public editQuizForm: FormGroup;
  @ViewChild('closebutton', { static: false }) closebutton: { nativeElement: { click: () => void; }; };
  dataResponse: any = [];
  savedQuiz: any;
  tokenValue = this.storageService.get('token');
  constructor(private fb: FormBuilder, private storageService: QuizStorageService,
    private httpService: QuizHttpService, private sweetAlert: SweetAlertService
    , private router: Router, private SpinnerService: NgxSpinnerService) { }

  ngOnInit() {

    this.initForm();
    this.viewQuiz();
    this.enagment_type();

  }
  initForm() {
    this.addQuizForm = this.fb.group({
      numberOfQuestions: ['', [Validators.required]],
      quizType: ['', [Validators.required]],

    });
    this.editQuizForm = this.fb.group({
      numberOfQuestions: ['', [Validators.required]],


    });
  }


  list_engagement: any = [];
  enagment_type() {
    axios.get('http://env-9498608.cloudjiffy.net/api/engagement_type')
      .then((response) => {
        this.list_engagement = response.data.list_engagement_type;

        console.log(this.list_engagement, 'list_engagement');
      })
  }


  addQuizModel: any = {

    "quizType": '',
    "numberOfQuestions": '',
    "engagment_id": ''

  }

  save() {

    const reqMap = {
      ...this.addQuizForm.value,


    };
    if (reqMap.quizType == "1") {
      reqMap['quizType'] = "corporate";
      reqMap['engagment_id'] = "1";

    }
    else if (reqMap.quizType == "2") {
      reqMap['quizType'] = "educate";
      reqMap['engagment_id'] = "2";
    }
    console.log(reqMap, ' ...this.addQuizForm.value');
    console.log(this.addQuizModel, ' ...this.addQuizForm.value');
    axios.post('http://env-9498608.cloudjiffy.net/api/add_quiz', reqMap)
      .then((response) => {

        if (response.data.code == 200) {
          this.addQuizModel.quizType = '';
          this.addQuizModel.noOfQuestion = '';
          this.addQuizModel.engagment_id = '';
          this.savedQuiz = response;

          console.log(this.savedQuiz.success, 'check');

          this.closebutton.nativeElement.click();
          this.sweetAlert.swalSuccess('Quiz Added Successfully')
          this.viewQuiz();
        }
        else if (response.data.code != 200) {
          this.sweetAlert.swalError('error occurred');
        }
      })
      .catch(function (error) {
        console.log(error);
      });

    this.addQuizForm.reset();

  }
  quizId: any;
  quizid(id: any) {
    this.quizId = id;
    console.log(id, 'quizid');
  }
  editQuiz() {


    const reqMap = {
      ...this.editQuizForm.value,


    };
    let edit = {
      "id": this.quizId,
      "no_of_ques": reqMap.numberOfQuestions
    }

    console.log(reqMap.numberOfQuestions, ' ...this.editQuizForm.value');

    axios.post('http://env-9498608.cloudjiffy.net/api/edit_quiz', edit)
      .then((response) => {

        if (response.data.code == 200) {

          this.closebutton.nativeElement.click();
          this.sweetAlert.swalSuccess('Quiz Added Successfully')
          this.viewQuiz();
        }
        else if (response.data.code != 200) {
          this.sweetAlert.swalError('error occurred');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    this.quizId = '';
    this.editQuizForm.reset();
    this.closebutton.nativeElement.click();
  }



  viewQuiz() {
    this.SpinnerService.show();

    axios.get('http://env-9498608.cloudjiffy.net/api/list_quiz')
      .then((response) => {

        if (response.data.code == 200) {
          this.dataResponse = response.data.list_quiz;
          console.log(this.dataResponse, 'check');
          this.SpinnerService.hide();

        }
        else if (response.data.code != 200) {
          this.sweetAlert.swalError('error occurred');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  deleteQuiz(emp: any) {
    console.log(emp, "id")
    const reqMap = {
      "id": emp

    };
    axios.post('http://env-9498608.cloudjiffy.net/api/delete_quiz', reqMap)
      .then((response) => {
        if (response.data.code == 200) {
          this.dataResponse = response.data.list_quiz;
          this.viewQuiz();
        }


      });

    this.viewQuiz();
  }
  quizType() {

    axios.get('http://env-9498608.cloudjiffy.net/api/list_quizType')
      .then((response) => {

      })
      .catch(function (error) {
        console.log(error);
      });
  }

  viewQuestion(emp) {
    this.quizId = emp,
      this.storageService.set('quizID', this.quizId);
    console.log(this.quizId, 'quizId');
    this.router.navigate(['/admin/manageQuestion'])
  }
  roleModel: any = {
    "id": 0,
    "type": ""
  }
  slectedValue: any;

  onChange(event: any) {
    console.log('event', event);
    console.log('roleModel', this.roleModel.id);
    console.log('roleModel', this.roleModel.type);

  }

}
