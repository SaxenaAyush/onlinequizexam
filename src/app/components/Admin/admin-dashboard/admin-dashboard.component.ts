import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { Router } from '@angular/router';
import * as Highcharts from 'highcharts';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import axios from 'axios';
import * as moment from 'moment';
import { HttpHeaders, HttpRequest, HttpInterceptor, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  @ViewChild('closebutton', { static: false }) closebutton;
  GraphDataMode: any = {
    'Education': '',
    'coparate': '',
    'edu': [],
    'cop': []
  }
  singleAudience: any;
  multipleAudience: any;
  tokenValue = this.storageService.get('token');
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column',
    },
    title: {
      text: ''
    },
    legend: {
      //  layout: 'vertical',
      //  align: 'left',
      //  verticalAlign: 'top',
      x: 250,
      y: 100,
      floating: true,
      borderWidth: 1,

    },
    xAxis: {
      categories: ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    },
    yAxis: {
      min: 0, title: {
        text: 'Edu Institute'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },

    series: [
      {
        name: "Edu-Institute",
        data: [28, 48, 40, 19, 86, 27, 90, 33, 55, 55, 23, 77]
      }

    ]
  }
  chartOptions2 = {
    chart: {
      type: 'column',
    },
    title: {
      text: ''
    },
    legend: {
      //  layout: 'vertical',
      //  align: 'left',
      //  verticalAlign: 'top',
      x: 250,
      y: 100,
      floating: true,
      borderWidth: 1,

    },
    xAxis: {
      categories: ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    },
    yAxis: {
      min: 0, title: {
        text: 'Corporate'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },

    series: [
      {
        name: "Corporate",
        data: [28, 48, 40, 19, 86, 27, 90, 33, 55, 55, 23, 77]
      }

    ]
  }
  @ViewChild('security', { static: true }) securityInput: any;
  disabled = false;
  ShowFilter = false;
  limitSelection = false;

  dropdownList = [];
  selectedItems = [];

  dropdownSettings = {};
  public userForm: FormGroup;

  constructor(private storageService: QuizStorageService, private router: Router,
    private fb: FormBuilder, private SpinnerService: NgxSpinnerService,
    private sweetAlert: SweetAlertService, private quizHttpService: QuizHttpService) { }

  ngOnInit() {

    this.dashboard();
    this.quiz_audience_options();
    this.enagment_type();
    this.initForm();
    this.storageService.get('token');

    console.log(this.tokenValue, 'tokenValues')
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });








    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: false
    };
  }

  droplist: any = [];
  quiz_audience_options() {
    axios.get('http://env-9498608.cloudjiffy.net/api/quiz_audience')
      .then((response) => {
        this.droplist = response.data.list_quiz_audience;
        this.dropdownList = [];
        for (let opt of this.droplist) {
          this.dropdownList.push({
            'item_id': opt.id,
            'item_text': opt.quiz_audience
          })
        }


      })
  }

  list_engagement: any = [];
  enagment_type() {
    axios.get('http://env-9498608.cloudjiffy.net/api/engagement_type')
      .then((response) => {
        this.list_engagement = response.data.list_engagement_type;
        console.log(this.list_engagement, 'list_engagement');
      })
  }

  gettoken() {
    let head = new HttpHeaders().set(
      "Authorization",
      this.storageService.get('token')
    );
  }
  random: any
  initForm() {
    this.userForm = this.fb.group({
      location_of_quiz: ['', [Validators.required]],
      date_visit: ['', [Validators.required]],
      security: [this.random, [Validators.required]],
      quiz_audience: ['', [Validators.required]],
      expected_part: ['', [Validators.required]],
      course: ['', [Validators.required]],

    });
  }


  dashboardData: any;
  dashboardlist = [];
  perfomanceSnapshotData: any;
  perfomanceModel: any = {
    'tp': '',
    'cp': '',
    'ep': ''
  }
  GraphData: any;


  serialno: any = 1;
  graphEducation: number[];
  educationChart: any;
  corprateChart: any;
  dashboard() {
    this.SpinnerService.show();

    axios.get('http://env-9498608.cloudjiffy.net/api/home_data', { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.dashboardData = response.data.Data;
        this.perfomanceSnapshotData = response.data.Data.perfomanceSnapshot;
        this.perfomanceModel.tp = this.perfomanceSnapshotData.totalPartcipante;
        this.perfomanceModel.cp = this.perfomanceSnapshotData.Cp;
        this.perfomanceModel.ep = this.perfomanceSnapshotData.EP;
        this.dashboardlist = this.dashboardData.list;
        this.GraphData = this.dashboardData.GraphData;

        this.GraphDataMode.Education = this.GraphData.Education;
        this.GraphDataMode.coparate = this.GraphData.coparate;
        for (let key of this.GraphDataMode.Education) {
          for (var i in key) {

            this.GraphDataMode.edu.push(
              key[i]
            )

          }
        }


        for (let key of this.GraphDataMode.coparate) {
          for (var i in key) {

            this.GraphDataMode.cop.push(
              key[i]
            )

          }
        }




        this.chartOptions.series[0].data = this.GraphDataMode.edu;
        this.educationChart = this.chartOptions;
        this.chartOptions2.series[0].data = this.GraphDataMode.cop;
        this.corprateChart = this.chartOptions2;
        this.SpinnerService.hide();


      })
  }

  viewEngagment: any;
  view_engagment() {

    this.SpinnerService.show();

    axios.get('http://env-9498608.cloudjiffy.net/api/view_engagement', { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.viewEngagment = response.data.View_engagement;
        this.storageService.set('anagment', this.viewEngagment);
        console.log(this.viewEngagment, 'view anagment');
        // console.log(this.perfomanceSnapshotData, 'perfomanceSnapshotData');
        // console.log(this.dashboardlist, 'dashboardlist'); 
      })
    this.SpinnerService.hide();

  }
  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
    }
  }

  onItemSelect(item: any) {
    console.log('onItemSelect', item);
    this.singleAudience = item
  }
  onSelectAll(items: any) {
    console.log('onSelectAll', items);
    this.multipleAudience = items
  }
  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
  }
  randomString() {
    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let length = 6;
    var result = '';
    for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    this.securityInput.nativeElement.value = result;
    this.random = result
    console.log('result', this.random)

  }
  dataResponse: any;
  save() {
    //   httpOptions.headers =
    //   httpOptions.headers.set('Authorization', this.tokenValue);
    //  let data= new FormData(),key;
    //  data.append('token', this.tokenValue);


    //  let token = httpOptions.headers;


    const reqMap = {
      ...this.userForm.value,

    };

    var course = new String("");
    var str2 = new String(",");
    for (let i of reqMap.course) {
      var str1 = new String("");
      str1 = i.item_text;
      course = course + i.item_text + ',';


      //   course = i.item_text ;
      //  console.log(i.item_text, i)
    }

    reqMap.course = course;

    //  data = {
    // ... reqMap
    //  }

    //  for(var val of data.entries()){
    //    console.log(val[0]+ ""+val[1]);
    //  }
    // data.forEach((value,key) => {
    //   console.log(key+" "+value)
    // });
    //  console.log(reqMap,'reqMap');
    reqMap['date_visit'] = moment(reqMap['date_visit']).format('YYYY-MM-DD');
    reqMap['security'] = this.random;

    axios.post('http://env-9498608.cloudjiffy.net/api/add_engagement', reqMap, { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {

        this.dataResponse = response;
        this.closebutton.nativeElement.click();
        this.sweetAlert.swalSuccess('User Added Successfully')

      })
      .catch(function (error) {
        console.log(error);
      });

  }

}
