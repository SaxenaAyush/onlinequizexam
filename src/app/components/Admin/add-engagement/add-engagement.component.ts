import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { Router } from '@angular/router';
import * as Highcharts from 'highcharts';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import axios from 'axios';
import * as moment from 'moment';
import { HttpHeaders, HttpRequest, HttpInterceptor, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-engagement',
  templateUrl: './add-engagement.component.html',
  styleUrls: ['./add-engagement.component.css']
})
export class AddEngagementComponent implements OnInit {
  public userForm: FormGroup;
  @ViewChild('closebutton', { static: false }) closebutton;
  singleAudience: any;
  multipleAudience: any;
  tokenValue = this.storageService.get('token');
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column',
    },
    title: {
      text: ''
    },
    legend: {
      //  layout: 'vertical',
      //  align: 'left',
      //  verticalAlign: 'top',
      x: 250,
      y: 100,
      floating: true,
      borderWidth: 1,

    },
    xAxis: {
      categories: ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    },
    yAxis: {
      min: 0, title: {
        text: 'Edu Institute'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },

    series: [
      {
        name: "Edu-Institute",
        data: [28, 48, 40, 19, 86, 27, 90, 33, 55, 55, 23, 77]
      }

    ]
  }
  chartOptions2 = {
    chart: {
      type: 'column',
    },
    title: {
      text: ''
    },
    legend: {
      //  layout: 'vertical',
      //  align: 'left',
      //  verticalAlign: 'top',
      x: 250,
      y: 100,
      floating: true,
      borderWidth: 1,

    },
    xAxis: {
      categories: ['Jan', 'Feb', 'March', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
    },
    yAxis: {
      min: 0, title: {
        text: 'Corporate'
      }
    },
    plotOptions: {
      column: {
        dataLabels: {
          enabled: true
        }
      }
    },

    series: [
      {
        name: "Corporate",
        data: [28, 48, 40, 19, 86, 27, 90, 33, 55, 55, 23, 77]
      }

    ]
  }
  @ViewChild('security', { static: true }) securityInput: any;
  disabled = false;
  ShowFilter = false;
  limitSelection = false;

  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  constructor(private storageService: QuizStorageService, private router: Router, private fb: FormBuilder,
    private sweetAlert: SweetAlertService, private quizHttpService: QuizHttpService) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      phone_no: ['', [Validators.required]],
      user_type: ['', [Validators.required]],
      is_active: true
    });
    this.initForm();
    this.storageService.get('token');

    console.log(this.tokenValue, 'tokenValues')
    const $button = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');

    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });

  }

  initForm() {

    this.dropdownList = [
      { item_id: 1, item_text: 'CA' },
      { item_id: 2, item_text: 'BCOM ' },
      { item_id: 3, item_text: 'MBAs' },
      { item_id: 4, item_text: 'CMA ' },
      { item_id: 5, item_text: 'ICWAI ' },
      { item_id: 6, item_text: ' MBA (Finance)' },
      { item_id: 4, item_text: 'Engineering graduate ' },
      { item_id: 5, item_text: 'BBA ' },

    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: false
    };
  }

  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
    }
  }



  dataResponse: any;
  save() {



    const reqMap = {
      ...this.userForm.value

    };

    // var course  = new String("");
    // var str2  = new String(",");
    //   for(let i of reqMap.course){
    //     var str1  = new String("");
    //     str1=i.item_text;
    //     course=course + i.item_text + ',';

    //  }

    //  reqMap.course=course;


    reqMap['date_visit'] = moment(reqMap['date_visit']).format('YYYY-MM-DD');

    axios.post('http://env-9498608.cloudjiffy.net/api/add_engagement', reqMap, { headers: { "token": ` ${this.tokenValue}` } })
      .then((response) => {
        this.dataResponse = response;


      })
      .catch(function (error) {
        console.log(error);
      });
    this.userForm = null;
  }
  save2() {
    const reqMap = {
      ...this.userForm.value,

    };
    // console.log(reqMap)
    axios.post('http://env-9498608.cloudjiffy.net/api/register', reqMap)
      .then((response) => {
        this.dataResponse = response;

        console.log(this.dataResponse.success, 'check');

        this.closebutton.nativeElement.click();
        this.sweetAlert.swalSuccess('User Added Successfully')

      })
      .catch(function (error) {
        console.log(error);
      });

  }

}

