import { Component, OnInit } from '@angular/core';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { ChartOptions, ChartType, ChartDataSets, Chart } from 'chart.js';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import axios from 'axios';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-manage-quiz',
  templateUrl: './manage-question.component.html',
  styleUrls: ['./manage-question.component.css']
})
export class ManageQuestionComponent implements OnInit {
  public editQuestionForm: FormGroup
  questions = [
    {

      "name": "Which HTML tag do we use to put the JavaScript?",

      "options": [
        {

          "name": "<javascript>",

        },
        {

          "name": "<script>",

        },
        {

          "name": "<js>",

        },
        {

          "name": "None of the above",

        }
      ],

    },
    {

      "name": "Which HTML tag do we use to put the JavaScript?",

      "options": [
        {

          "name": "<javascript>",

        },
        {

          "name": "<script>",

        },
        {

          "name": "<js>",

        },
        {

          "name": "None of the above",

        }
      ],
      "questionType": {

        "name": "Multiple Choice",

      }
    },
    {

      "name": "Which HTML tag do we use to put the JavaScript?",

      "options": [
        {

          "name": "<javascript>",

        },
        {

          "name": "<script>",

        },
        {

          "name": "<js>",

        },
        {

          "name": "None of the above",

        }
      ],
      "questionType": {

        "name": "Multiple Choice",

      }
    },
    {

      "name": "Which HTML tag do we use to put the JavaScript?",

      "options": [
        {

          "name": "<javascript>",

        },
        {

          "name": "<script>",

        },
        {

          "name": "<js>",

        },
        {

          "name": "None of the above",

        }
      ],
      "questionType": {

        "name": "Multiple Choice",

      }
    },

  ]



  option: any
  constructor(private fb: FormBuilder, private storageService: QuizStorageService,
    private httpService: QuizHttpService, private sweetAlert: SweetAlertService,
    private SpinnerService: NgxSpinnerService) { }
  loggedInUserDetails: any;
  dataResponse: any = [];
  quizid = this.storageService.get('quizID');
  questionName: any;
  optionName: any;
  ngOnInit() {
    this.editQuestionForm = this.fb.group({
      question: ['', [Validators.required]],
      option1: ['', [Validators.required]],
      option2: ['', [Validators.required]],
      option3: ['', [Validators.required]],
      option4: ['', [Validators.required]],

    });
    // this.data = res.json()['results'];
    for (let que of this.questions) {
      this.questionName = que.name;
      for (let u of que.options) {
        this.optionName = u.name
      }
    }
    this.viewQuestion();
    console.log('idddd', this.quizid);
    this.editQuestionForm.patchValue({
      question: "dssdd",
      option1: this.optionName,
      option2: this.optionName,
      option3: this.optionName,
    });
  }
  viewQuestion() {
    const reqMap = {
      "quizid": this.quizid
    }
    this.SpinnerService.show();
    axios.post('http://env-9498608.cloudjiffy.net/api/view_question', reqMap)
      .then((response) => {

        if (response.data.code == 200) {
          this.dataResponse = response.data.View_Question;
          console.log(this.dataResponse, 'check');

          this.SpinnerService.hide();
        }
        else if (response.data.code != 200) {
          this.sweetAlert.swalError('error occurred');
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }


  deleteQuiz(emp: any) {
    console.log(emp, "id")
    const reqMap = {
      "id": emp

    };
    axios.post('http://env-9498608.cloudjiffy.net/api/delete_question', reqMap)
      .then((response) => {
        if (response.data.code == 200) {
          this.dataResponse = response.data.View_Question;
          this.viewQuestion();
        }


      });

  }
}

