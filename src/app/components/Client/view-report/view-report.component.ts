import { Component, OnInit } from '@angular/core';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-report',
  templateUrl: './view-report.component.html',
  styleUrls: ['./view-report.component.css']
})
export class ViewReportComponent implements OnInit {
status = 0;
  constructor(private storageService :QuizStorageService,private router:Router) { }
  loggedInUserDetails:any;
  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('currentUser');
    const $button  = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');
    
    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });
  }
  Results(){
this.status = 1;
  }
  logout(){
    this.router.navigate(['/home'])
  }
}
