import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../../node_modules/jquery';
import { Router } from '@angular/router';
import {FormGroup, FormBuilder, Validators, FormControl, NgForm} from '@angular/forms';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.css']
})
export class ClientDashboardComponent implements OnInit {

  Radiotext:any;
  questionmodel : any = {
    ques : 'Question',
    ans: 'Ans.',
    no: 1,
    page: 'Page',
    quesdes :'What is the use of angular js ?',
    flag :0,
    optionA : '',
    optionB : '',
    optionC : '',
    optionD: ''

  }

  checkboxmodel :any = {
    A :1,
    B:1,
    C:1,
    D:1
  }
  progressbar : any = {
    dynamic : 10,
    max : 30,
    totalques: 10
  }
 
  confirm(){
    this.router.navigate(['client/confirm']);
  }

  
  nextques(){
    this.Radiotext = false
    if(this.progressbar.dynamic<100){
    var value= Math.floor(100/this.progressbar.totalques);
    let check=this.progressbar.dynamic+value;
    if(check>100){
     
    }else{
      this.progressbar.dynamic=this.progressbar.dynamic+value;
    }
    
    }

    if(this.questionmodel.flag<this.progressbar.totalques){
      this.resetColor();
      this.Radiotext= false
      this.questionmodel.flag=this.questionmodel.flag+1;
     
        if(this.questionmodel.flag==1){
          this.questionmodel.ques= 'What is income?'
          this.questionmodel.quesdes= 'What is income?'
          this.questionmodel.optionA='1. Principal of a loan.';
          this.questionmodel.optionB='2. The date on which a loan must be paid back in full.';
          this.questionmodel.optionC='3. Money received as a result of business activity; a financial gain.';

          this.questionmodel.no=1;
        }else if(this.questionmodel.flag==2){
          this.questionmodel.quesdes= 'What does appreciation mean?'
          this.questionmodel.optionA='1. The measure of a continued rise in the worth of an asset.';
          this.questionmodel.optionB='2. Current Assets.';
          this.questionmodel.optionC='3. The decrease in value of an asset over time.';
          this.questionmodel.no=2;
        }else if(this.questionmodel.flag==3){
          this.questionmodel.quesdes= 'What is a Balloon Loan?'
          this.questionmodel.optionA='1. A loan for which one significant lump sum payment is due at maturity.';
          this.questionmodel.optionB='2. Fixed-interest loan.';
          this.questionmodel.optionC='3. A short term financing.';

          this.questionmodel.no=3;
        }else if(this.questionmodel.flag==4){
          this.questionmodel.quesdes= 'What is a Buy Back?'
          this.questionmodel.optionA='1. Buyback is a stock related term that refers to the buying back of stocks or bonds by the issuing company.';
          this.questionmodel.optionB='2. Initial Public Offering.';
          this.questionmodel.optionC='3. Issuing new company shares.';

          this.questionmodel.no=4;
        }else if(this.questionmodel.flag==5){
          this.questionmodel.quesdes= 'What is a Balance Sheet?'
          this.questionmodel.optionA='1. Balancing cash flow and net earnings.';
          this.questionmodel.optionB='2. A financial statement of assets, liabilities and owners’ equity for a business at a certain point of time.';
          this.questionmodel.optionC='3. The balance between price and earnings.';

          this.questionmodel.no=5;
        }else if(this.questionmodel.flag==6){
          this.questionmodel.quesdes= 'What is a Balloon Loan?'
          this.questionmodel.optionA='1. A loan for which one significant lump sum payment is due at maturity.';
          this.questionmodel.optionB='2. Fixed-interest loan.';
          this.questionmodel.optionC='3. A short term financing.';

          this.questionmodel.no=6;
        }else if(this.questionmodel.flag==7){
          this.questionmodel.quesdes= 'What is a Bad Credit Loan?'
          this.questionmodel.optionA='1. Money borrowed from a financial institution with bad reputation. Unpredictable market.';
          this.questionmodel.optionB='2. A loan designed for people or business with bad credit.';
          this.questionmodel.optionC='3. A loan with principal, which doesn’t have to be repaid until maturity';

          this.questionmodel.no=7;
        }else if(this.questionmodel.flag==8){
          this.questionmodel.quesdes= 'What is Assumable Mortgage?'
          this.questionmodel.optionA='1. A fixed-rate mortgage.';
          this.questionmodel.optionB='2. Adjustable Rate Mortgage.';
          this.questionmodel.optionC='3. A mortgage that can be transferred to another borrower.';

          this.questionmodel.no=8;
        }else if(this.questionmodel.flag==9){
          this.questionmodel.quesdes= 'What is bridge loan?'
          this.questionmodel.optionA='1. A short-term loan made until a longer term financing is obtain.';
          this.questionmodel.optionB='2. Adjustable Rate Mortgage.';
          this.questionmodel.optionC='3. A commercial loan for building a bridge.';

          this.questionmodel.no=9;
        }else if(this.questionmodel.flag==10){
          this.questionmodel.quesdes= 'What is capital?'
          this.questionmodel.optionA='1. Company’s liabilities.';
          this.questionmodel.optionB='2. Company expenses.';
          this.questionmodel.optionC='3. Any assets that can be used for production of further assets.';

          this.questionmodel.no=10;
        }
      
    }
   
  }
  submit(no){
console.log('Value',no)
  }

  prevques(){
    if(this.progressbar.dynamic>0){
      var value= Math.floor(100/this.progressbar.totalques);
      this.progressbar.dynamic=this.progressbar.dynamic-value;
    }
  
    if(this.questionmodel.flag>0){
      this.resetColor();
      this.questionmodel.flag=this.questionmodel.flag-1;
     
      if(this.questionmodel.flag==1){
        this.questionmodel.quesdes= 'What is income?'
        this.questionmodel.optionA='1. Principal of a loan.';
        this.questionmodel.optionB='2. The date on which a loan must be paid back in full.';
        this.questionmodel.optionC='3. Money received as a result of business activity; a financial gain.';

        this.questionmodel.no=1;
      }else if(this.questionmodel.flag==2){
        this.questionmodel.quesdes= 'What does appreciation mean?'
        this.questionmodel.optionA='1. The measure of a continued rise in the worth of an asset.';
        this.questionmodel.optionB='2. Current Assets.';
        this.questionmodel.optionC='3. The decrease in value of an asset over time.';

        this.questionmodel.no=2;
      }else if(this.questionmodel.flag==3){
        this.questionmodel.quesdes= 'What is a Balloon Loan?'
        this.questionmodel.optionA='1. A loan for which one significant lump sum payment is due at maturity.';
        this.questionmodel.optionB='2. Fixed-interest loan.';
        this.questionmodel.optionC='3. A short term financing.';

        this.questionmodel.no=3;
      }else if(this.questionmodel.flag==4){
        this.questionmodel.quesdes= 'What is a Buy Back?'
        this.questionmodel.optionA='1. Buyback is a stock related term that refers to the buying back of stocks or bonds by the issuing company.';
        this.questionmodel.optionB='2. Initial Public Offering.';
        this.questionmodel.optionC='3. Issuing new company shares.';

        this.questionmodel.no=4;
      }else if(this.questionmodel.flag==5){
        this.questionmodel.quesdes= 'What is a Balance Sheet?'
        this.questionmodel.optionA='1. Balancing cash flow and net earnings.';
        this.questionmodel.optionB='2. A financial statement of assets, liabilities and owners’ equity for a business at a certain point of time.';
        this.questionmodel.optionC='3. The balance between price and earnings.';

        this.questionmodel.no=5;
      }else if(this.questionmodel.flag==6){
        this.questionmodel.quesdes= 'What is a Balloon Loan?'
        this.questionmodel.optionA='1. A loan for which one significant lump sum payment is due at maturity.';
        this.questionmodel.optionB='2. Fixed-interest loan.';
        this.questionmodel.optionC='3. A short term financing.';

        this.questionmodel.no=6;
      }else if(this.questionmodel.flag==7){
        this.questionmodel.quesdes= 'What is a Bad Credit Loan?'
        this.questionmodel.optionA='1. Money borrowed from a financial institution with bad reputation. Unpredictable market.';
        this.questionmodel.optionB='2. A loan designed for people or business with bad credit.';
        this.questionmodel.optionC='3. A loan with principal, which doesn’t have to be repaid until maturity';

        this.questionmodel.no=7;
      }else if(this.questionmodel.flag==8){
        this.questionmodel.quesdes= 'What is Assumable Mortgage?'
        this.questionmodel.optionA='1. A fixed-rate mortgage.';
        this.questionmodel.optionB='2. Adjustable Rate Mortgage.';
        this.questionmodel.optionC='3. A mortgage that can be transferred to another borrower.';

        this.questionmodel.no=8;
      }else if(this.questionmodel.flag==9){
        this.questionmodel.quesdes= 'What is bridge loan?'
        this.questionmodel.optionA='1. A short-term loan made until a longer term financing is obtain.';
        this.questionmodel.optionB='2. Adjustable Rate Mortgage.';
        this.questionmodel.optionC='3. A commercial loan for building a bridge.';

        this.questionmodel.no=9;
      }else if(this.questionmodel.flag==10){
        this.questionmodel.quesdes= 'What is capital?'
        this.questionmodel.optionA='1. Company’s liabilities.';
        this.questionmodel.optionB='2. Company expenses.';
        this.questionmodel.optionC='3. Any assets that can be used for production of further assets.';

        this.questionmodel.no=10;
      }
    }
  }
  Questions = [
    {
     QnID :"1",
     Qn: "What is your name?", 
     Option1: "Ayush", 
     Option2: 31, 
     Option3: 31, 
     Option4: 31, 
     },
     {
      QnID :"2",
      Qn: "What is your office name?", 
      Option1: "31", 
      Option2: "Mohshri", 
      Option3: "31", 
      Option4: "31", 
      },
      {
        QnID :"3",
        Qn: "What is your home Town name?", 
        Option1: "31",
        Option2: "Bareilly", 
        Option3: "31", 
        Option4: "31",
         

        },
        {
          QnID :"4",
          Qn: "What is your Current Address?", 
          Option1: "31", 
          Option2: "Indrapuram", 
          Option3: "31", 
          Option4: "31", 
          },
          {
            QnID :"5",
            Qn: "What is your Friend name?", 
            Option1: "31", 
            Option2: "31", 
            Option3: "31", 
            Option4: "Anshul", 
            },
    ];
    questionNo = 0;
  answerSelected: any;
  questionArray: any[] = [
    {
      'primaryKey': 337,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 352,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'assets/images/cssquestion.jpg',
        'type': 'png',
        'headerPk': 337,
        'saveNo': 1,
        'imageStorePath': '../../../assets/images/cssquestion.jpg'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 112,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 113,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': '../../../assets/images/learning-slider-image.jpg',
        'type': 'png',
        'headerPk': 112,
        'saveNo': 1,
        'imageStorePath': '../../../assets/images/notes.png'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 133,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 135,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': '../../../assets',
        'type': 'png',
        'headerPk': 133,
        'saveNo': 1,
        'imageStorePath': '../../../assets/images/recruitment.png'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 323,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 335,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': '../../../assets/images/glenn-carstens-peters-203007-unsplash.jpg',
        'type': 'png',
        'headerPk': 323,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 130,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 132,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': '../../../assets/images/exam.png',
        'type': 'png',
        'headerPk': 130,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 135,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 137,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_137_1.png',
        'type': 'png',
        'headerPk': 135,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 129,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 131,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_131_1.png',
        'type': 'png',
        'headerPk': 129,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 127,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 128,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_128_1.png',
        'type': 'png',
        'headerPk': 127,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 118,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 119,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_119_1.png',
        'type': 'png',
        'headerPk': 118,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 345,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 359,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_359_1.png',
        'type': 'png',
        'headerPk': 345,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 120,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 121,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_121_1.png',
        'type': 'png',
        'headerPk': 120,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 333,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 346,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_346_1.png',
        'type': 'png',
        'headerPk': 333,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 139,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 141,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_141_1.png',
        'type': 'png',
        'headerPk': 139,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 336,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 350,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_350_1.png',
        'type': 'png',
        'headerPk': 336,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 124,
      'answer': 'B',
      'eoImage': {
        'primaryKey': 125,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_125_1.png',
        'type': 'png',
        'headerPk': 124,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 320,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 332,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_332_1.png',
        'type': 'png',
        'headerPk': 320,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 322,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 334,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_334_1.png',
        'type': 'png',
        'headerPk': 322,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 326,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 338,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_338_1.png',
        'type': 'png',
        'headerPk': 326,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 121,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 122,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_122_1.png',
        'type': 'png',
        'headerPk': 121,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 137,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 139,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_139_1.png',
        'type': 'png',
        'headerPk': 137,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 316,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 328,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_328_1.png',
        'type': 'png',
        'headerPk': 316,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 122,
      'answer': 'D',
      'eoImage': {
        'primaryKey': 123,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_123_1.png',
        'type': 'png',
        'headerPk': 122,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 119,
      'answer': 'A',
      'eoImage': {
        'primaryKey': 120,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_120_1.png',
        'type': 'png',
        'headerPk': 119,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 318,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 330,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_330_1.png',
        'type': 'png',
        'headerPk': 318,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 319,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 330,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_330_1.png',
        'type': 'png',
        'headerPk': 318,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 319,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 330,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_330_1.png',
        'type': 'png',
        'headerPk': 318,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
    {
      'primaryKey': 320,
      'answer': 'C',
      'eoImage': {
        'primaryKey': 330,
        'entityName': 'EOQuestion',
        'displayName': null,
        'imageUrl': 'EOQuestion_330_1.png',
        'type': 'png',
        'headerPk': 318,
        'saveNo': 1,
        'imageStorePath': '/opt/tomcat/webapps/ImgData/onlinetest'
      },
      'lkExamType': {
        'primaryKey': 1,
        'examType': 'Java',
        'duration': 30
      }
    },
  ];
  totalQuestions = this.questionArray.length;
  examDuration = 1000 * 180;
  tileArray = [];

  constructor(private router:Router) { }

  resetColor(){
    // var optionA = document.getElementById("optionA");
    // optionA.style.backgroundColor="#E1DBDC"

    // var optionAtxt = document.getElementById("optionAtxt");
    // optionAtxt.style.color="#9D9899";
    // optionAtxt.style.fontSize ='17px';

    // var A = document.getElementById("A");
    // A.style.fontSize ='24px';


    // var optionB = document.getElementById("optionB");
    // optionB.style.backgroundColor="#E1DBDC"

    // var optionBtxt = document.getElementById("optionBtxt");
    // optionBtxt.style.color="#9D9899"
    // optionBtxt.style.fontSize ='17px';

    // var B = document.getElementById("B");
    // B.style.fontSize ='23px';


    // var optionC = document.getElementById("optionC");
    // optionC.style.backgroundColor="#E1DBDC"

    // var optionCtxt = document.getElementById("optionCtxt");
    // optionCtxt.style.color="#9D9899"
    // optionCtxt.style.fontSize ='17px';

    // var C = document.getElementById("C");
    // C.style.fontSize ='23px';


    // var optionD = document.getElementById("optionD");
    // optionD.style.backgroundColor="#E1DBDC"

    // var optionDtxt = document.getElementById("optionDtxt");
    // optionDtxt.style.color="#9D9899"
    // optionDtxt.style.fontSize ='23px';

    // var D = document.getElementById("D");
    // D.style.fontSize ='23px';

   
  }
  // changeColorA(){
  //   var optionA = document.getElementById("optionA");
  //   optionA.style.backgroundColor="#FC264D"

  //   var optionAtxt = document.getElementById("optionAtxt");
  //   optionAtxt.style.color="#FBFCFC";
  //   optionAtxt.style.fontSize ='20px';



  //   var optionB = document.getElementById("optionB");
  //   optionB.style.backgroundColor="#E1DBDC"

  //   var optionBtxt = document.getElementById("optionBtxt");
  //   optionBtxt.style.color="#9D9899"
  //   optionBtxt.style.fontSize ='17px';



  //   var optionC = document.getElementById("optionC");
  //   optionC.style.backgroundColor="#E1DBDC"

  //   var optionCtxt = document.getElementById("optionCtxt");
  //   optionCtxt.style.color="#9D9899"
  //   optionCtxt.style.fontSize ='17px';


   
  // }
  // changeColorB(){
  //   var optionA = document.getElementById("optionA");
  //   optionA.style.backgroundColor="#E1DBDC"

  //   var optionAtxt = document.getElementById("optionAtxt");
  //   optionAtxt.style.color="#9D9899"
  //   optionAtxt.style.fontSize ='20px';




  //   var optionB = document.getElementById("optionB");
  //   optionB.style.backgroundColor="#FC264D"

  //   var optionBtxt = document.getElementById("optionBtxt");
  //   optionBtxt.style.color="#FBFCFC"
  //   optionBtxt.style.fontSize ='17px';



  //   var optionC = document.getElementById("optionC");
  //   optionC.style.backgroundColor="#E1DBDC"

  //   var optionCtxt = document.getElementById("optionCtxt");
  //   optionCtxt.style.color="#9D9899"

  //   optionCtxt.style.fontSize ='17px';



  // }

  // changeColorC(){
  //   var optionA = document.getElementById("optionA");
  //   optionA.style.backgroundColor="#E1DBDC"

  //   var optionAtxt = document.getElementById("optionAtxt");
  //   optionAtxt.style.color="#9D9899"

  //   optionAtxt.style.fontSize ='20px';


  //   var optionB = document.getElementById("optionB");
  //   optionB.style.backgroundColor="#E1DBDC"

  //   var optionBtxt = document.getElementById("optionBtxt");
  //   optionBtxt.style.color="#9D9899"

  //   optionBtxt.style.fontSize ='17px';




  //   var optionC = document.getElementById("optionC");
  //   optionC.style.backgroundColor="#FC264D"

  //   var optionCtxt = document.getElementById("optionCtxt");
  //   optionCtxt.style.color="#FBFCFC"

  //   optionCtxt.style.fontSize ='17px';


   
  // }

  // changeColorD(){
  //   var optionA = document.getElementById("optionA");
  //   optionA.style.backgroundColor="#E1DBDC"

  //   var optionAtxt = document.getElementById("optionAtxt");
  //   optionAtxt.style.color="#9D9899"

  //   optionAtxt.style.fontSize ='23px';

  //   var A = document.getElementById("A");
  //   A.style.fontSize ='23px';


  //   var optionB = document.getElementById("optionB");
  //   optionB.style.backgroundColor="#E1DBDC"

  //   var optionBtxt = document.getElementById("optionBtxt");
  //   optionBtxt.style.color="#9D9899"

  //   optionBtxt.style.fontSize ='23px';

  //   var B = document.getElementById("B");
  //   B.style.fontSize ='23px';


  //   var optionC = document.getElementById("optionC");
  //   optionC.style.backgroundColor="#E1DBDC"

  //   var optionCtxt = document.getElementById("optionCtxt");
  //   optionCtxt.style.color="#9D9899"

  //   optionCtxt.style.fontSize ='23px';

  //   var C = document.getElementById("C");
  //   C.style.fontSize ='23px';


  //   var optionD = document.getElementById("optionD");
  //   optionD.style.backgroundColor="#FC264D"

  //   var optionDtxt = document.getElementById("optionDtxt");
  //   optionDtxt.style.color="#FBFCFC"

  //   optionDtxt.style.fontSize ='24px';

  //   var D = document.getElementById("D");
  //   D.style.fontSize ='24px';

   
  // }


  
//   changeCheckA(){

//     if(this.checkboxmodel.A==1){
//       var optionA = document.getElementById("optionCA");
//       optionA.style.backgroundColor="#FC264D"
  
//       var optionAtxt = document.getElementById("optionCAtxt");
//       optionAtxt.style.color="#FBFCFC";
//       optionAtxt.style.fontSize ='24px';
  
//       var A = document.getElementById("CA");
//       A.style.fontSize ='24px';
//       this.checkboxmodel.A=2
//          }else if(this.checkboxmodel.A==2){
//           this.checkboxmodel.A=1;
//       var optionA = document.getElementById("optionCA");
//       optionA.style.backgroundColor="#E1DBDC"         
  
//       var optionAtxt = document.getElementById("optionCAtxt");
//       optionAtxt.style.color="#9D9899";
//       optionAtxt.style.fontSize ='23px';
  
//       var A = document.getElementById("CA");
//       A.style.fontSize ='23px';
//     }
  


    
//   }

//   changeCheckB(){

   
//  if(this.checkboxmodel.B==1){
//     var optionB = document.getElementById("optionCB");
//     optionB.style.backgroundColor="#FC264D"

//     var optionBtxt = document.getElementById("optionCBtxt");
//     optionBtxt.style.color="#FBFCFC";
//     optionBtxt.style.fontSize ='24px';

//     var B = document.getElementById("CB");
//     B.style.fontSize ='24px';
//     this.checkboxmodel.B=2;
//  }
//  else if(this.checkboxmodel.B==2){
//   this.checkboxmodel.B=1;
//   var optionB = document.getElementById("optionCB");
//   optionB.style.backgroundColor="#E1DBDC"

//   var optionBtxt = document.getElementById("optionCBtxt");
//   optionBtxt.style.color="#9D9899";
//   optionBtxt.style.fontSize ='23px';

//   var B = document.getElementById("CB");
//   B.style.fontSize ='23px';

//  }

    
   
//   }
//   changeCheckC(){
   
//     if(this.checkboxmodel.C==1){
//     var optionC = document.getElementById("optionCC");
//     optionC.style.backgroundColor="#FC264D"

//     var optionCtxt = document.getElementById("optionCCtxt");
//     optionCtxt.style.color="#FBFCFC";
//     optionCtxt.style.fontSize ='24px';

//     var C = document.getElementById("CC");
//     C.style.fontSize ='24px';
//     this.checkboxmodel.C=2;
//     }
//     else if(this.checkboxmodel.C==2){
//       this.checkboxmodel.C=1;
//       var optionC = document.getElementById("optionCC");
//       optionC.style.backgroundColor="#E1DBDC"
  
//       var optionCtxt = document.getElementById("optionCCtxt");
//       optionCtxt.style.color="#9D9899";
//       optionCtxt.style.fontSize ='23px';
  
//       var C = document.getElementById("CC");
//       C.style.fontSize ='23px';
//     }


   
   
//   }
//   changeCheckD(){

  
//     if(this.checkboxmodel.D==1){
//     var optionD = document.getElementById("optionCD");
//     optionD.style.backgroundColor="#FC264D"

//     var optionDtxt = document.getElementById("optionCDtxt");
//     optionDtxt.style.color="#FBFCFC";
//     optionDtxt.style.fontSize ='24px';

//     var D = document.getElementById("CD");
//     D.style.fontSize ='24px';
//     this.checkboxmodel.D=2;
//     }else if(this.checkboxmodel.D==2){
//       this.checkboxmodel.D=1;
//       var optionD = document.getElementById("optionCD");
//       optionD.style.backgroundColor="#E1DBDC"
  
//       var optionDtxt = document.getElementById("optionCDtxt");
//       optionDtxt.style.color="#9D9899";
//       optionDtxt.style.fontSize ='23px';
  
//       var D = document.getElementById("CD");
//       D.style.fontSize ='23px';
//     }

   
//   }
  ngOnInit() {
    this.nextques();

    
  }
  next() {
    this.questionNo = this.questionNo + 1;
  }
 previous() {
    this.questionNo = this.questionNo - 1;
  }
  logout(){
this.router.navigate(['/home'])
  }
}
