import { Component, OnInit } from '@angular/core';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import * as $ from '../../../../node_modules/jquery';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { QuizHttpService } from 'src/app/service/quiz-http.service';
import { SweetAlertService } from 'src/app/common/sweetalert2.service';
import axios from 'axios';

@Component({
  selector: 'app-manage-account',
  templateUrl: './manage-account.component.html',
  styleUrls: ['./manage-account.component.css']
})
export class ManageAccountComponent implements OnInit {
  loggedInUserDetails:any;

  passwordModel : any = {
    'password' : '',
    'confirm' : '',
  }
  constructor(private router: Router, private route: Router, private fb: FormBuilder,
    private storageService : QuizStorageService, private http:HttpClient,  private quizHttpService: QuizHttpService, private alertService: SweetAlertService,) { }


  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('currentUser');
console.log('value',this.loggedInUserDetails);
const $button  = document.querySelector('#sidebar-toggle');
const $wrapper = document.querySelector('#wrapper');

$button.addEventListener('click', (e) => {
  e.preventDefault();
  $wrapper.classList.toggle('toggled');
});
  }
  ChangePassword(){

  console.log(this.storageService.get('userdetail'));
  let user=this.storageService.get('userdetail');
  let email=user.email;
  console.log(email);
if(this.passwordModel.password!=''){
  if(this.passwordModel.confirm!=''){
if(this.passwordModel.password==this.passwordModel.confirm){

  const reqMap = {
      "email":email,
      "password":this.passwordModel.password
  }
  axios.post('http://env-9498608.cloudjiffy.net/api/update_password', reqMap)
  .then((res) => {
        if(res.data.code==200){
this.alertService.swalSuccess("Sucessfully Login!");
  this.passwordModel.password='';
  this.passwordModel.confirm='';
        }else if(res.data.code!=200){
          this.alertService.swalError("**password not changed Try Again**");
        }
  })
  .catch(function (error) {
    console.log(error);
  });
}else{
  this.alertService.swalError("**Password & Confirm Password not matched**");
  
}
  }else{
    this.alertService.swalError("Recquired Confirm Password!");
  }

}else{
  this.alertService.swalError("Recquired Password!");
}
  }
}
