import { Component, OnInit } from '@angular/core';
import * as $ from '../../../../node_modules/jquery';
import { Router } from '@angular/router';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  constructor(private router: Router, private storageService: QuizStorageService) { }
  loggedInUserDetails: any;
  
  
  ngOnInit() {
    
    const $button  = document.querySelector('#sidebar-toggle');
    const $wrapper = document.querySelector('#wrapper');
    
    $button.addEventListener('click', (e) => {
      e.preventDefault();
      $wrapper.classList.toggle('toggled');
    });


  }
  checkClass(value){
    if(this.router.url == value) return true; else return false;
  }
  logout(){
    localStorage.removeItem('currentUser');
    this.router.navigate(['/homeAdmin']);

  }
}
