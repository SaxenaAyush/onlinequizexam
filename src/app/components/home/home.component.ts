import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from '../../../../node_modules/jquery';
import { QuizStorageService } from 'src/app/service/quiz-storage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  loginModel : any = {
    email : '',
    phone : '',
    password: '',
    code : '',
    error : ''
  }
  customOptions: any = {
    items: 1,
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    itemClass: 'media media-pointer',
    dots: false,
    navSpeed: 1500,
    nav: false,
    navigation: false,
    navText: ['<img src=\'assets/images/icons/backward45.png\'>', '<img src=\'assets/images/icons/forward45.png\'>'],
    autoplay: true,
    autoplayHoverPause: true
  };
  slidesStore = [
    {
      src: 'assets/images/test4.jpg',
      title: 'Lets create outstanding AdvertiseMent'
    },
    {
      src: 'assets/images/banner-img1.jpg',
      title: 'Create your own Adds'
    },
    {
      src: 'assets/images/test3.jpg',
      title: 'Its Our Promise'
    },
  ];
  status = 0;
  userList = [
    {
      id: 'admin',
      password: 12345
    },
    {
      id: 'user',
      password: 12345
    }
  ];
  public loginForm: FormGroup
  constructor(private router: Router, private route: Router, private fb: FormBuilder,
    private storageService : QuizStorageService) { }

  ngOnInit() {
    this.initForm();
    $(document).ready(function() {
      $("#burger-container").on("click", function() {
        $("#burger-container").toggleClass("open"),
          $("nav ul").toggleClass("show"),
          $(".content").toggleClass("move"); //onclick 'this' -> burger-container toggles with a class of 'open'
      });
    });
  }
  initForm() {
    this.loginForm = this.fb.group({
      emailID: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
  login(){
   
        const reqMap = {
          emailID: this.loginModel.email,
          password: this.loginModel.password
            };

console.log(this.loginModel);
            if(this.loginModel.email == 'user'){
              if(this.loginModel.phone== 54321){
                if( this.loginModel.password == 12345){
                  if( this.loginModel.code == 2323){
                    this.loginModel.error='';
                    this.router.navigate(['client/instruction']);
                    this.storageService.set('currentUser',reqMap);
                  }else if(this.loginModel.code==''){
                    this.loginModel.error = '**Security Code Required! **';
                    $(function () {
                      $("#code").focus();
                      
                    }); 
                   
                  }else if(this.loginModel.code!=2323){
                    this.loginModel.error = '** Security Code is Incorrect! **';
                    $(function () {
                      $("#code").focus();
                      
                    }); 
                   
                  }
                }else if(this.loginModel.password == ''){
                  this.loginModel.error = '** Password Required! **';
                  $(function () {
                    $("#pwd").focus();
                    
                  }); 
                 
                }else if(this.loginModel.password != 12345){
                  this.loginModel.error = ' ** Password is Incorrect! **';
                  $(function () {
                    $("#pwd").focus();
                    
                  }); 
                 
                }
              }else if(this.loginModel.phone==''){
                this.loginModel.error = ' ** Phone Number Required! **';
                $(function () {
                  $("#phone").focus();
                  
                }); 
               
              }else if(this.loginModel.phone!=54321){
                this.loginModel.error = '** Phone Number is  Incorrect! **';
                $(function () {
                  $("#phone").focus();
                  
                }); 
               
              }
              
            }else if(this.loginModel.email==''){
              this.loginModel.error = ' ** Email Required! **';
              $(function () {
                $("#email").focus();
                
              }); 
             
            }else if(this.loginModel.email!='user'){
              this.loginModel.error = '** Email is Incorrect! **';
              $(function () {
                $("#email").focus();
                
              }); 
             
            }
                
    
            //     console.log("else working");
            //   }
    
            // }  
            // }
    
    }

open() {
    $('#mySidebar').show(); 
}

 close() {
  $('#mySidebar').hide();
}
logout(){
  this.router.navigate(['/home']);

}
PageLogin(){
  this.router.navigate(['/homeAdmin'])
}
}
